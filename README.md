# Service Registration
Source: https://github.com/Java-Techie-jt/spring-cloud-gatway-hystrix

Team: Lisa Maria Panzl, Lukas Leitner

This service just starts the discovery-server alias registration service!

## Step 1: Enable discovery client in your own service

Other services must include ``@EnableDiscoveryClient`` at the start of there application class.

Example for our api-gateway:
```
# api-gateway\src\main\java\sst\alpha\apigatewayApiGatewayApplication.java
...

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ApiGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiGatewayApplication.class, args);
    }

}

```
#### build.gradle:
if your application is a gradle, and not a marven one, you need to include following in your ``build.gradle``:

```
# build.gradle
...

repositories {
    ...
    maven { url 'https://repo.spring.io/milestone' }
}

ext {
    set('springCloudVersion', "Hoxton.SR1")
}

dependencies {
    implementation 'org.springframework.cloud:spring-cloud-starter-netflix-eureka-client'
    implementation 'org.springframework.cloud:spring-cloud-starter-netflix-eureka-server'
    ...
}

dependencyManagement {
    imports {
        mavenBom "org.springframework.cloud:spring-cloud-dependencies:${springCloudVersion}"
    }
}


```

## Step 2: Give your app a name
Your application name should be defined in your ``application.properties``, like:
```
# api-gateway\src\main\resources\application.properties
spring.application.name=<your-name>

```

Our api-gateway, needs to set your name in the routing configuration, so <br>
it will redirect the upcoming request successfully to your service (aka. RestController / Mapping).
<br>For more information go to our api-gateway

And the discovery-server saves your application name for verification of the registration process.

## Verify your registration
Your can verify your registration, but you don't have to. <br>

In the following code example you have a private ``DiscoveryClient`` attribute named ``discoveryClient``. <br>
With ``discoveryClient`` you can get an ``DiscoveryClient`` instance over your application name.  <br>

You will receive an empty list ``[]``, if your service is not registered.<br>
Otherwise, the list has a lot of information.  
<br> Please read the following example carefully. Specially the ``RequestMapping`` part
```
# a demo service...
# client-demo-registration\src\main\java\sst\alpha\cdr\CdrApplication.java
...

@SpringBootApplication
@EnableDiscoveryClient
public class CdrApplication {

    public static void main(String[] args) {
        SpringApplication.run(CdrApplication.class, args);
    }

}

# please write for RequestMapping '<your-application-name>/'
# It is your entrypoint for all requests
@RequestMapping("client-demo-registration") 
@RestController
class ServiceInstanceRestController {

    @Autowired
    private DiscoveryClient discoveryClient;

    @GetMapping("/app/{applicationName}")
    public List<ServiceInstance> serviceInstancesByApplicationName(
            @PathVariable String applicationName) {
        return this.discoveryClient.getInstances(applicationName);
    }
}

```

## Port
This service will use the port ``8761``


